#!/usr/bin/env python

# Run `pcscd --foreground --apdu 2>&1 | tee pcscd-apdu-request-response.txt`
# to capture APDUs. This utility converts them to the format that is suitable
# for insertion into the request-to-response pair list in a card emulator.

import sys, collections, re

REPLACEMENT_TUPLES = [
    ('41 30 30 30 30 30 30 30 31',       'A00000001',   '123456789'),
    ('33 30 30 30 30 30 30 30 30 30 30', '30000000000', '12345678901'),
    ('45 45 53 54 49 20 2F 20 45 53 54', 'EESTI / EST', 'HUNGARY/HUN'),
]

Replacement = collections.namedtuple('Replacement', ['re', 'replacement'])

REPLACEMENTS = []

def _to_hexpairs(hexbytes):
    return ' '.join(map(''.join, zip(*[iter(hexbytes)] * 2)))

for rt in REPLACEMENT_TUPLES:
    REPLACEMENTS.append(Replacement(re.compile(rt[0]),
                                    _to_hexpairs(rt[2].encode('hex'))))

def main(filename, handler):
    requests = []
    responses = []
    handler = globals()[handler]
    with open(filename) as f:
        for count, line in enumerate(f):
            hexbytes = _replace(line.split(':')[1].strip())
            if count % 2:
                responses.append(hexbytes)
            else:
                requests.append(hexbytes)
    for pairs in zip(requests, responses):
        handler(*pairs)

def humanreadable(request, response):
    print '%s: %s' % (response, repr(_to_bytes(response)))

def pythonformat(request, response):
    print "'%s': '%s'," % (_to_pythonformat(request),
                             _to_pythonformat(response))

def cppformat(request, response):
    print '{{%s}, {%s}},' % (_to_cppformat(request),
                             _to_cppformat(response))

def longcppformat(request, response):
    print '{{%s}, {%s}}, // %s' % (_to_cppformat(request),
                                   _to_cppformat(response),
                                   repr(_to_bytes(response)))

def _replace(hexbytes):
    for replacement in REPLACEMENTS:
        hexbytes = replacement.re.sub(replacement.replacement, hexbytes)
    return hexbytes

def _to_pythonformat(hexbytes):
    return hexbytes.replace(' ', '')

def _to_cppformat(hexbytes):
    return '0x' + hexbytes.replace(' ', ', 0x')

def _to_bytes(hexbytes):
    return hexbytes.replace(' ', '').decode('hex')

if __name__ == '__main__':
    input_filename = sys.argv[1] if len(sys.argv) > 1 else 'pcscd-apdu-request-response.txt'
    action = sys.argv[2] if len(sys.argv) > 2 else 'pythonformat'
    main(input_filename, action)
