# PCSC-Lite driver for emulating smart card readers in software

This library provides a PC/SC IFD handler for emulating smart card readers in
software.

The driver can be used for implementing software-based security tokens that
simulate smart cards or for testing smart card software.

The project was created during the [hackEST](http://hackest.org/) event in
September 2013 and has been recently updated in September 2018.

## Estonian eID card simulation

The following example shows the Estonian eID card utility reading card data
from a simulated card:

![image](https://bitbucket.org/mrts/pcsclite-softemu-ifdhandler/raw/master/doc/software-emulated-EstEID-card.png)

This was achieved by

- capturing the communication APDUs with a real Estonian eID card using `pcscd` APDU mode as
described below,
- transforming the APDUs with the `utils/apduparser.py` utility,
- filling in the ATR and request-response APDUs in `utils/libsoftemu-ifdhandler.py`.

## How does it work

`pcscd` loads the `libsoftemu-ifdhandler.so` driver that by default executes
`/usr/local/bin/libsoftemu-ifdhandler.run` and sends ATR and APDU requests to
it via standard input and reads responses from standard output.

`libsoftemu-ifdhandler.run` can be any application or script that works
according to the protocol, but a sample Python script is provided in
`utils/libsoftemu-ifdhandler.py` and installed by `make install`.

Alternatively, it is possible to avoid using an external application by using
`RequestResponseStepsCardEmulator` instead of `ExternalApplicationCardEmulator`
in `SmartCardEmulator.cpp` and filling the `requestResponsePairs` list in
`cards/RequestResponseStepsCardEmulator.cpp`.

## Building and activating the libsoftemu-ifdhandler reader

Install `pcscd`, CMake, C++ build tools, *Boost.Process* and *Boost.System*
libraries first:

    sudo apt install pcscd build-essential cmake libboost-dev libboost-system-dev

To build, activate and test the software-based reader driver:

    cd softemu-ifdhandler
    make
    sudo su
    make install

    service pcscd stop
    pcscd --foreground --debug --apdu --color 2>&1 | tee pcsd-log.txt

## Emulating custom cards

To emulate custom cards, implement the following functions in `utils/libsoftemu-ifdhandler.py`:

    def atr(argument):
        # return card ATR here

    def apdu(request):
        # return response APDUs for request APDUs here

## Debugging

This is only needed during low-level development.

To see how `pcscd` interacts with the driver in debugger (requires debug mode `pcscd` build):

    apt-get source pcscd
    sudo service pcscd stop
    sudo cgdb --directory=pcsc-lite-1.8.6/src --args /usr/sbin/pcscd --foreground --debug
    (gdb) break RFInitializeReader
    (gdb) run

To compile and install `pcscd` in debug mode without optimizations:

    cd pcsc-lite-1.8.6
    export DEB_BUILD_OPTIONS='nostrip noopt'
    dpkg-buildflags # verify that -g and -O0 is in the flags
    dpkg-buildpackage -us -uc
    sudo dpkg -i ../libpcsclite-dbg_1.8.6-3ubuntu1b1_amd64.deb
    sudo dpkg -i ../pcscd_1.8.6-3ubuntu1b1_amd64.deb
