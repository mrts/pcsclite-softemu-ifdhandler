#include "SmartCardEmulatorBase.h"

#include <stdexcept>

void SmartCardEmulatorBase::outputAtr(PUCHAR Atr, PDWORD AtrLength)
{
    if (!Atr)
        throw std::invalid_argument("Null ATR buffer");

    auto atrBytes = atr();
    if (*AtrLength < atrBytes.size())
        throw std::invalid_argument("ATR buffer too small");

    *AtrLength = atrBytes.size();
    for (const auto byte : atrBytes) {
        *Atr = byte;
        ++Atr;
    }
}

void SmartCardEmulatorBase::responseForRequest(PUCHAR TxBuffer, DWORD TxLength,
                                               PUCHAR RxBuffer, PDWORD RxLength)
{
    if (!TxBuffer)
        throw std::invalid_argument("Null request buffer");
    if (!RxBuffer)
        throw std::invalid_argument("Null response buffer");

    byte_vector request(TxBuffer, TxBuffer + TxLength);

    auto response = responseForRequestImpl(request);

    // no response
    if (response.size() < 1) {
        if (*RxLength > 1) {
            // 68 00 - requested function not supported
            RxBuffer[0] = 0x68;
            RxBuffer[1] = 0x00;
            *RxLength = 2;
        }
        return;
    }

    // small buffer
    if (*RxLength < response.size()) {
        if (*RxLength > 1) {
            // 67 00 - length incorrect
            RxBuffer[0] = 0x67;
            RxBuffer[1] = 0x00;
            // FIXME: AFAICR should send response.size() in RxLength
            // when length incorrect, but have to check this
            *RxLength = 2;
        }
        return;
    }

    // return response
    *RxLength = response.size();
    for (const auto byte : response) {
        *RxBuffer = byte;
        ++RxBuffer;
    }
}
