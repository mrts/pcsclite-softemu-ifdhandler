#pragma once

#include <vector>
#include <string>
#include <iomanip>
#include <sstream>
#include <stdexcept>

typedef std::vector<unsigned char> byte_vector;

inline std::string byteVectorToHexString(const byte_vector& bytes)
{
    std::ostringstream hexStringBuilder;

    hexStringBuilder << std::setfill('0') << std::hex;

    for (const auto byte : bytes) {
        hexStringBuilder << std::setw(2) << static_cast<short>(byte);
    }

    return hexStringBuilder.str();
}

inline unsigned char hexToChar(char hex)
{
    if (hex >= '0' && hex <= '9') return hex - '0';
    if (hex >= 'a' && hex <= 'f') return hex - 'a' + 10;
    if (hex >= 'A' && hex <= 'F') return hex - 'A' + 10;
    throw std::runtime_error("hexToChar: hex character must be in range [0-9] or [a-f]");
}

inline byte_vector byteVectorFromHexString(const std::string& str)
{
    if (str.length() % 2 != 0) {
        throw std::runtime_error("byteVectorFromHexString: hex string must have even length");
    }

    auto response = byte_vector(str.length() / 2, 0x00);
    for (size_t i = 0; i < response.size(); ++i) {
        response[i] = (hexToChar(str[i*2]) << 4) | hexToChar(str[i*2 + 1]);
    }

    return response;
}
