#ifndef SMARTCARDEMULATOR_H__
#define SMARTCARDEMULATOR_H__

#include "pcscdefines.h"

class SmartCardEmulator
{
public:
    static SmartCardEmulator& instance();

    virtual ~SmartCardEmulator() = default;

    virtual void outputAtr(PUCHAR Atr, PDWORD AtrLength) = 0;
    virtual void responseForRequest(PUCHAR TxBuffer, DWORD TxLength,
                            PUCHAR RxBuffer, PDWORD RxLength) = 0;

protected:
    SmartCardEmulator() = default;

private:
    SmartCardEmulator(const SmartCardEmulator&) = delete;
    SmartCardEmulator& operator=(const SmartCardEmulator&) = delete;
};

#endif /* SMARTCARDEMULATOR_H */
