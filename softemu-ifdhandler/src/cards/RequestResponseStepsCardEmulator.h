#pragma once

#include "../SmartCardEmulatorBase.h"

class RequestResponseStepsCardEmulator : public SmartCardEmulatorBase
{
public:
    using SmartCardEmulatorBase::SmartCardEmulatorBase;
    virtual ~RequestResponseStepsCardEmulator() = default;

private:
    virtual byte_vector atr();
    virtual byte_vector responseForRequestImpl(const byte_vector& request);

    unsigned int _step = 0;
};
