#include "ExternalApplicationCardEmulator.h"

#include <iostream>

namespace {

const char* const EXTERNAL_APPLICATION = "/usr/local/bin/libsoftemu-ifdhandler.run";

}

namespace bp = boost::process;

ExternalApplicationCardEmulator::ExternalApplicationCardEmulator() :
    app_input{},
    app_output{},
    app{EXTERNAL_APPLICATION, bp::std_out > app_output, bp::std_in < app_input, bp::shell},
    atr_buf{}
{
}

ExternalApplicationCardEmulator::~ExternalApplicationCardEmulator()
{
    app.terminate();
}

byte_vector ExternalApplicationCardEmulator::atr()
{
    if (atr_buf.empty()) {
        checkIfIsRunning();
        app_input << "atr:" << std::endl;
        atr_buf = getResponseFromOutput();
    }
    return atr_buf;
}

byte_vector ExternalApplicationCardEmulator::responseForRequestImpl(const byte_vector& request)
{
    checkIfIsRunning();
    app_input << "apdu:" << byteVectorToHexString(request) << std::endl;
    return getResponseFromOutput();
}

byte_vector ExternalApplicationCardEmulator::getResponseFromOutput()
{
    std::string response;
    app_output >> response;
    return byteVectorFromHexString(response);
}

void ExternalApplicationCardEmulator::checkIfIsRunning()
{
    if (!app.running()) {
        throw std::runtime_error(std::string(EXTERNAL_APPLICATION) + " is not running");
    }
}
