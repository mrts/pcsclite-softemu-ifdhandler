#include "SmartCardEmulator.h"
#include "cards/ExternalApplicationCardEmulator.h"
#include "cards/RequestResponseStepsCardEmulator.h"

SmartCardEmulator& SmartCardEmulator::instance()
{
    // change manually if you want to use RequestResponseStepsCardEmulator instead
    static ExternalApplicationCardEmulator cardEmulatorInstance;
    return cardEmulatorInstance;
}
