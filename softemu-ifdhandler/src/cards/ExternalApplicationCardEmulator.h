#pragma once

#include "../SmartCardEmulatorBase.h"

#include <boost/process.hpp>

class ExternalApplicationCardEmulator : public SmartCardEmulatorBase
{
public:
    ExternalApplicationCardEmulator();
    virtual ~ExternalApplicationCardEmulator();

private:
    virtual byte_vector atr();
    virtual byte_vector responseForRequestImpl(const byte_vector& request);

    byte_vector getResponseFromOutput();
    void checkIfIsRunning();

    boost::process::opstream app_input;
    boost::process::ipstream app_output;
    boost::process::child app;

    byte_vector atr_buf;
};
