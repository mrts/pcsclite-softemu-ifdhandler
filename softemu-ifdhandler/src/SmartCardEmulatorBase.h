#ifndef SMARTCARDEMULATORBASE_H__
#define SMARTCARDEMULATORBASE_H__

#include "SmartCardEmulator.h"

#include "byte_vector.h"

class SmartCardEmulatorBase : public SmartCardEmulator
{
public:
    virtual void outputAtr(PUCHAR Atr, PDWORD AtrLength);
    virtual void responseForRequest(PUCHAR TxBuffer, DWORD TxLength,
                            PUCHAR RxBuffer, PDWORD RxLength);

protected:
    SmartCardEmulatorBase() = default;
    virtual ~SmartCardEmulatorBase() = default;

private:
    // template methods
    virtual byte_vector atr() = 0;
    virtual byte_vector responseForRequestImpl(const byte_vector& request) = 0;
};

#endif /* SMARTCARDEMULATORBASE_H */
